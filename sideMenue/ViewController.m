//
//  ViewController.m
//  sideMenue
//
//  Created by Prince on 18/11/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *simpleView;
@property (strong, nonatomic) IBOutlet UIButton *okbutton;

@end

@implementation ViewController
@synthesize simpleView;
- (void)viewDidLoad {
    simpleView.hidden=TRUE;
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)okpressed:(id)sender {
    
   // [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
  //  [UIView setAnimationTransition:UIViewAutoresizingFlexibleBottomMargin forView:simpleView cache:YES];
    
    [UIView animateWithDuration:6.0
                          delay:0.1
                        options: UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^{
                         simpleView.frame=CGRectMake(0, 0, 375, 530);
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"finish");
                     }];
    
    
    
    simpleView.hidden=FALSE;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
